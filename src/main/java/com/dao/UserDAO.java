package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Users;

@Service
public class UserDAO {

    @Autowired
    private UserRepository userRepository;

    public Users registerUser(Users user) {
        return userRepository.save(user);
    }

    public boolean authenticateUser(String email, String password) {
        Users user = userRepository.findByEmailIdAndPassword(email, password);
        return user != null;
    }
    
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }
}
