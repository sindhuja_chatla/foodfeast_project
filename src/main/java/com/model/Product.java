package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
	  @Id
	  @GeneratedValue
      private int id;
      private String productName;
      private String description;
      private String category;
      private double price;
      private String imgUrl;
      
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(int id, String productName, String description, String category, double price, String imgUrl) {
		super();
		this.id = id;
		this.productName = productName;
		this.description = description;
		this.category = category;
		this.price = price;
		this.imgUrl = imgUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", description=" + description + ", category="
				+ category + ", price=" + price + ", imgUrl=" + imgUrl + "]";
	}
      
	
      
    
     
}
